**Readme Ex9:**

Created on 24/04/2020 by William FLEITH


**Objective :** In this exercice we will build a virtual environment. The main point of a virtual env is to work with and on a specific env for every project. It will enable to crash and destroy it the rebuild a new one if we did a mistake instead of having trouble finding where is the problem on our system and having issues with it.

**Building and Setting up a virtual env :**
- go in the repository where you want to build it. 
- if u have done it any time install virtual env linux commmand
	- Sudo apt install virtualenv
- Create the virtual env : virtualenv venv
- Go to your virtual env : source venv/bin/activate
- You can now work in it.
- To go out : deactivate






# -*- coding: utf-8 -*-
"""
Created on 03/04/2020
by William FLEITH
"""
from setuptools import setup
import setuptools

setup(
    name='TestSimpleCalculator',
    version='0.0.1',
    author="Williamfleith",
    packages=["calculator", "calculator/"],
    description="TestSimpleCalculator is a simple package \
    in order to make some test on packaging principles in Python",
    license='GNU GPLv3',
    python_requires ='>=3.4',
)

